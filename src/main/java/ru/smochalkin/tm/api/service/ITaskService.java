package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task project);

    void remove(Task project);

    List<Task> findAll();

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String desc);

    Task updateByIndex(Integer index, String name, String desc);

}
