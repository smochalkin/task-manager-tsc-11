package ru.smochalkin.tm.api.controller;

import ru.smochalkin.tm.model.Project;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

    void showById();

    void showByName();

    void showByIndex();

    void showProject(Project project);

    void removeById();

    void removeByName();

    void removeByIndex();

    void updateById();

    void updateByIndex();

}
