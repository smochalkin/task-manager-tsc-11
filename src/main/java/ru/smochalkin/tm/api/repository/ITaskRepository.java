package ru.smochalkin.tm.api.repository;

import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String desc);

    Task updateByIndex(Integer index, String name, String desc);

    int getCount();

}
